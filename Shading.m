% Ran by read.m
hold on;
%% Shading

num_phases = length(index_lpbegin); % same as lpend

% y_max = max(data);
base = min_y;
shading_upper = base + 1;
y_lims = [shading_upper shading_upper];
for i = 1:num_phases
    light = area([t(index_lpbegin(i)) t(index_lpend(i))],y_lims,base);
    light.FaceColor = [0 0.7461 0.95];
    if i == num_phases
        deep = area([t(index_lpend(i)) t(end)],y_lims);
        deep.FaceColor = [0 0 0.75];
        break;
    else
        deep = area([t(index_lpend(i)) t(index_lpbegin(i+1))],y_lims,base);
        deep.FaceColor = [0 0 0.75];
    end

end

deep.FaceColor = [0 0 0.75];

%% Lines indicating beginning and ending of each light phase cycle

% yL = get(gca,'YLim');
% Don't want yucky lines in the clean (user-friendly) figure
if clean ~= 1
    yL = [base shading_upper];
    for i = 1:num_phases
        phase_begin = t(index_lpbegin(i));
        line([phase_begin phase_begin],yL,'Color','g','Marker','*','LineStyle','--','LineWidth',2);
        phase_end = t(index_lpend(i));
        line([phase_end phase_end],yL,'Color','r','Marker','*','LineStyle','--','LineWidth',2);
    end
end

% figure;
% light = area([4 6], [10 10]);
% hold on;
% deep = area([6 8], [10 10]);
% light.FaceColor = [0 0 0.95];
% deep.FaceColor = [0 0 0.75];
% axis([0 10 0 10]);
% xa = 0:4;
% xb = linspace(4,6,10);
% xc = linspace(6,8,10);
% xd = 8:10;
% ya = xa*0 + 5;
% yb = 5*sin(10*xb)+5;
% yc = sin(10*xc)+5;
% yd = xd*0 + 5;
% 
% xdummy = [xa xb xc xd]; ydummy = [ya yb yc yd];
% 
% hold on; p = plot(xdummy,ydummy,'y');
% p.LineWidth = 3;
% 
% % Y = [1:]';
% % 
% % h = area(Y);
% 
% 

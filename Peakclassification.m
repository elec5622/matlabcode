% Ran by read.m


%% Peak detection
% clear; close all;
% load processed

% run after DSPexperiments



% h = openfig('Untitled');

threshold = 0.5; % set from inspection of processed data
peaks = data > threshold;

% user-friendly clutter free 
if clean ~= 1
    hold on; plot(t(1:length(peaks)),peaks*15,'r')
end

index_peaks = find(peaks);
num_peaks = length(index_peaks);

% Window for light phase window. Peaks deteceted within this window 
% are considered to be in the same light phase.
% 10-25 minutes of light sleep http://www.helpguide.org/articles/sleep/how-much-sleep-do-you-need.htm
window_upper = 25/60; 
window_lower = 10/60;

% Light phase window set
samppersec = 10; % data points per second, relevant to array index
secperhour = 3600; % seconds per hour
light_window_upper = window_upper * secperhour * samppersec; 
light_window_lower = window_lower * secperhour * samppersec; 


% arrays to describe light phase intervals
index_lpbegin = []; % beginning of light phase (= end of last deep phase)
index_lpend = []; % end of light phase (= beginning of next deep phase) 

% Find clusters of peaks lasting between window_lower and window_upper to 
% identify light phases.

this_index = 1; % Starting at the beginning
comp_index = this_index + 1; % Index of peak to compare this_index with
while this_index  < (num_peaks - 1)
    if check == 1
        disp('Current this_index')
        disp(index_peaks(this_index))
    end
    if this_index == comp_index
        comp_index = comp_index + 1;
    end
    % Sweep through peaks until comp_index is past the upper window. Two criteria have to be met for classifying sleep
    % phases: a cluster of peaks during 10-25 min of light phase sleep, and
    % ~1h of no peaks until the next light phase.
    while index_peaks(comp_index) - index_peaks(this_index) < light_window_upper
        if comp_index < num_peaks
            comp_index = comp_index + 1;
        else
            break
        end
    end
 
    prev_index = comp_index - 1;
    if check == 1
        disp('The peak at comp_index is the first index past upper window bound.')
        disp(index_peaks(comp_index))
        disp('The peak at prev_index is the last index within upper window bound.')
        disp('Now test whether it"s past lower window bound.')
        disp(index_peaks(prev_index))
    end
    % Check whether prev_index is further from this_index than lower window
    if index_peaks(prev_index) - index_peaks(this_index) > light_window_lower
        % The peak at prev_index is within the window from this_index. This
        % confirms both this_index as the start of a light phase and
        % prev_index as the end of that light phase.

        if this_index ~= prev_index
            index_lpbegin = [index_lpbegin index_peaks(this_index)]; % append starting index of this light phase
            index_lpend = [index_lpend index_peaks(prev_index) ]; % append starting index of next deep phase
        end
        if (check == 1) 
            disp('this_index appended to array of light phase beginnings index_lpbegin.')
            index_lpbegin
            disp('prev_index appended to array of light phase endings index_lpend.')
            index_lpend
        end
    else
        % The peak at prev_index occurs before the minimum duration to
        % classify the time interval [this_index prev_index] as a light
        % phase cycle. The absence of peaks in the 15 minutes between
        % prev_index and comp_index precludes the start of light phase 
        % from any peak in that interval.
        if (check == 1) 
            disp('Peaks between this_index and comp_index do not last up to 10-25 minutes. Discarded as noise.') 
        end
        % Do nothing since prev_index and this_index are just noise. 
    end
    if (check == 1) 
        disp('Start sweeping from comp_index, now set to this_index'); 
    end
    this_index = comp_index; 
end


% this_index = 1; % Starting at the beginning
% comp_index = this_index + 1; % Index of peak to compare this_index with
% while this_index  < (num_peaks - 1)
%     if check == 1
%         disp('Current this_index')
%         disp(index_peaks(this_index))
%     end
%     if this_index == comp_index
%         comp_index = comp_index + 1;
%     end
%     % Sweep through peaks until consecutive peaks are further apart than
%     % the set window. Two criteria have to be met for classifying sleep
%     % phases: a cluster of peaks during 10-25 min of light phase sleep, and
%     % ~1h of no peaks until the next light phase.
%     if index_peaks(comp_index) - index_peaks(this_index) > light_window_lower
%         if check == 1
%             disp('Found peak beyond lower window bound. this_index appended to array of light phase beginnings index_lpbegin.')
%             disp(index_peaks(comp_index))
%         end
%         % The interval between this_index and comp_index can be considered
%         % a light phase. this_index is the beginning of the light phase.
%         index_lpbegin = [index_lpbegin index_peaks(this_index)] % append starting index of this light phase
%         
%         % Determine whether comp_index is the end of this light phase.
%         % Keep sweeping along peaks until light_window_upper to determine the end 
%         % of this light phase, a.k.a. the beginning of the next deep phase. 
%         while index_peaks(comp_index) - index_peaks(this_index) < light_window_upper
%             comp_index = comp_index + 1;            
%             if check == 1
%                 disp('Sweeping comp_index for next peak outside window, for the next sleep cycle.')
%                 disp(index_peaks(comp_index))
%             end
%         end
%                 
%         % The previous peak is the end of the current light phase (which
%         % begins at this_index)   
%         if check == 1
%             disp('comp_index is beyond current light phase. Previous comp_index appended to array of light phase endings index_lpend.')
%             disp(index_peaks(comp_index))
%         end
%         if this_index ~= comp_index - 1
%             index_lpend = [index_lpend index_peaks(comp_index-1) ] % append starting index of next deep phase
%         end
%         % The peak at comp_index is possibly the  beginning of next light
%         % phase, to be checked in the next iteration.
%         this_index = comp_index; % start comparing from starting index of new light phase on next iteration
%         if check == 1
%             disp('New this_index set as comp_index')
%             disp(index_peaks(comp_index))
%         end
% 
%     else
%         % Possibly just noise, or the within of this light phase. Increment comp_index to keep
%         % sweeping.
%         if comp_index < num_peaks - 1
%             if check == 1
%                 disp('comp_index was within light phase window; keep sweeping')
%                 disp(index_peaks(comp_index))
%             end
%             comp_index = comp_index + 1;
%         else
% %             index_lpend = [index_lpend index_peaks(num_peaks)]
%             % index_lpend = circshift(index_lpend,-1,2); % put it in chronological order
%             break;
%         end
%     end
% end

if length(index_lpbegin) ~= length(index_lpend)
    index_lpbegin = index_lpbegin(1:length(index_lpend));
    index_lpend = index_lpend(1:length(index_lpbegin));
end
% % Find phases based on spacing
% this_index = 2; % Start at 2 because we already know the first one.
% comp_index = this_index + 1; % Peak to compare index with
% while this_index < (num_peaks - 1)
%     % Sweep through peaks until consecutive peaks are further apart than
%     % the set window. Two criteria have to be met for classifying sleep
%     % phases: a cluster of peaks during 10-25 min of light phase sleep, and
%     % ~1h of no peaks until the next light phase.
%     if index_peaks(comp_index) - index_peaks(this_index) > light_window_upper
%         % Index being compared is further away from current index than
%         % window_upper threshold. This can mean 1. comp_index is the beginning
%         % of the next light phase; 2. comp_index is part of the current
%         % (protracted) phase; 3. this_index was a bit of noise, if nearby
%         % peaks do not extend to the window_lower threshold.
%         % If there are no peaks 
%         % whether there is ~1h gap until the next peak (comp_index2). If there is, then
%         % comp_index is the end of the current light phase. Otherwise,
%         % adapt comp_index2 as comp_index.
%         index_lpbegin = [index_lpbegin index_peaks(comp_index)]; % append starting index of next light phase
%         index_lpend = [index_lpend index_peaks(comp_index-1) ]; % append starting index of next deep phase
%         this_index = comp_index; % start comparing from starting index of new light phase on next iteration
%     else
%         if comp_index < num_peaks
%             comp_index = comp_index + 1;
%         else
%             index_lpend = circshift(index_lpend,-1,2); % put it in chronological order
%             break;
%         end
%     end
% end


if check == 1
    index_lpbegin
    index_lpend
    phase_durations = index_lpend - index_lpbegin
end
if check_durations == 1
    phase_durations = index_lpend - index_lpbegin
end
    
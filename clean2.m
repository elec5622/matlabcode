%% Just plotting t and net acceleration

cycle = 1.5; % standard value taken for length of sleep cycle in hours
numcycles = ceil(hoursasleep/cycle);
intervals = linspace(0,numcycles*cycle,numcycles+1);

figure('units','normalized','outerposition',[0 0 1 1]);    
h = plot(t,net(1:length(t)),'b');
min_y = 0;
max_y = max(net) + 1; 
max_y = 14; % by observation
axis([min(t) max(t) min_y max_y]);
xlabel('Duration of sleep (h)');
ylabel('Activity (ms^-^2)');
%     legend('net activity','Location','northeast');
tlabel = ['Sleep Data ' tlabel];
title(tlabel);
ax = gca;
ax.XTick = intervals;
l = legend('Net acceleration');
set(l,'color','w','FontSize',16,'TextColor','black','LineWidth',2) 

grid on;

%% Just shading 
num_phases = length(index_lpbegin); % same as lpend

hold on;
% y_max = max(data);
base = 0;
shading_upper = mean(net)-1;
y_lims = [shading_upper shading_upper];
for i = 1:num_phases
    light = area([t(index_lpbegin(i)) t(index_lpend(i))],y_lims,base);
    light.FaceColor = [0 0.7461 0.95];
    if i == num_phases
        deep = area([t(index_lpend(i)) t(end)],y_lims);
        
        break;
    else
        deep = area([t(index_lpend(i)) t(index_lpbegin(i+1))],y_lims,base);
    end
        deep.FaceColor = [0 0 0.75];

end
% Find available Bluetooth devices.

 instrhwinfo('Bluetooth');
 instrhwinfo('Bluetooth', RemoteName); 
 
 ans.RemoteNames;

% Construct a Bluetooth object called b using channel 3 of a Lego Mindstorm robot with RemoteName of NXT.
% b = Bluetooth('NXT', 3);
b = Bluetooth('C3PO', 1);

% Connect to the remote device.

fopen(b);

% Send a message to the remote device using the fwrite function.

fwrite(b, uint8([2,0,1,155]));
b

% Read data from the remote device using the fread function.

name = fread(b,35);
acquire

% Disconnect the Bluetooth device.

 fclose(b);

% Clean up by deleting and clearing the object.

 fclose(b);
 clear(b);
 
%% More functions 

% binblockwrite % Write binblock data to instrument
% fgetl % Read line of text from instrument and discard terminator
% flushinput % Remove data from input buffer
% fopen % Connect interface object to instrument
% fread % Read binary data from instrument
% fwrite % Write binary data to instrument
% methods % Class method names and descriptions
% readasync % Read data asynchronously from instrument
% scanstr % Read data from instrument, format as text, and parse
% binblockread % Read binblock data from instrument
% fclose % Disconnect interface object from instrument
% fgets % Read line of text from instrument and include terminator
% flushoutput % Remove data from output buffer
% fprintf % Write text to instrument
% fscanf % Read data from instrument, and format as text
% query % Write text to instrument, and read data from instrument
% record % Record data and event information to file
% stopasync % Stop asynchronous read and write operations
load handel.mat

filename = 'handel.wav';
audiowrite(filename,y,Fs);
clear y Fs

% Read the data back into MATLAB using audioread.

[y,Fs] = audioread('handel.wav');

% Play the audio.

sound(y,Fs);

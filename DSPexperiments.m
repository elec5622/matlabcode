% Ran by read.m

% close all
% clear all;
%% Use this after read.m
% load data012;

x = net; n = t;

% plot(n(1:length(x)),x);

%% Signal Processing

% Taking inspiration from Pan-Tompkins algorithm (ECG analysis for QRS
% complexes)
% 
% ?	Bandpass ? reduce noise
% ?	Derivation ?emphasises R-peak
% ?	Squaring ? nonlinear amplification, mostly high f
% ?	Moving window integration ? waveform feature
% ?	Threshold
% ?	HR


%% Low Pass Filter
% 
% N   = 100;        % FIR filter order
% Fp  = 20e3;       % 20 kHz passband-edge frequency
% Fs  = 96e3;       % 96 kHz sampling frequency
% Rp  = 0.00057565; % Corresponds to 0.01 dB peak-to-peak ripple
% Rst = 1e-4;       % Corresponds to 80 dB stopband attenuation
% 
% NUM = firceqrip(N,Fp/(Fs/2),[Rp Rst],'passedge'); % NUM = vector of coeffs
% fvtool(NUM,'Fs',Fs,'Color','White') % Visualize filter
% 
% LPF = dsp.LowpassFilter('DesignForMinimumOrder',false,'FilterOrder',N,...
%     'PassbandFrequency',Fp,'SampleRate',Fs,'PassbandRipple',0.01,...
%     'StopbandAttenuation',80);
% tic
% while toc < 10
%     x = randn(256,1);
%     y = step(LPF,x);
%     step(SA,y);
% end
% 
% 
% % % 
% d = fdesign.highpass('Fp,Fst,Ap,Ast',3,5,0.5,40,40);
% Hd = design(d,'equiripple');
% x = filter(Hd,x);
% 
% pause(0.8); hold on; plot(n(1:length(x)),x);


%% Differentiation
% Good for accentuating movement ? significant changes 
% figure; plot(n,x);

% % Testing how many orders to differentiate
% max_order = 10;
% plot(n,x);
% % axis([1 9 0 60]);
% for order = 1:max_order
%     n = n(1:length(n)-1);
%     x = diff(x) + order*5; 
%     pause(0.8); hold on;; plot(n,x);
%     pause(0.8);
%     disp(order)
%     blah = max(x)/mean(x)
% end;
order = 2;
x = diff(x,order); 
n = n(1:(length(n)-order));
% pause(0.8); hold on;; plot(n(1:length(x)),x);

% pause(0.8); hold on;; plot(n,x_diff2);
% legend('Original','1st derivative','2nd derivative','3rd derivative');
% legend('Original','2nd derivative');

% grid on;

% n = 0:159;
% x = cos(pi/8*n)+cos(pi/2*n)+sin(3*pi/4*n);

%% Squaring
% Good for bringing negative data points to positive, further highlighting
% changes, and reducing noise (values <1)

x = x.^2;
% pause(0.8);

% Simplify the clean (user-friendly) figure
if clean ~= 1
    hold on; plot(n(1:length(x)),x,'m');
    disp('plot processed data')
end

% max_power = 3;
% % axis([1 9 0 60]);
% for power = 1:max_power
%     x = x.^power + power*10; 
%     pause(0.8); hold on;; semilogy(n,x);
%     pause(0.8);
%     disp(power)
%     blah = max(x)/mean(x)
% end;

% hdsp = plot(n(1:length(x)),x,'LineWidth',2,...
%     'MarkerSize',10,...
%     'MarkerEdgeColor','b',...
%     'MarkerFaceColor',[0.5,0.5,0.5]);
% % legend('Original','2nd derivative squared');

data = x; % processed data
t = n; % time (h)


% %% Bandpass filter
% 
% % Design an FIR equiripple bandpass filter to remove the lowest and highest discrete-time sinusoids.
% d = fdesign.bandpass('Fst1,Fp1,Fp2,Fst2,Ast1,Ap,Ast2',1/4,3/8,5/8,6/8,60,1,60);
% Hd = design(d,'equiripple');
% 
% % Apply the filter to the discrete-time signal.
% y = filter(Hd,x);
% freq = 0:(2*pi)/length(x):pi;
% xdft = fft(x);
% ydft = fft(y);
% plot(freq,abs(xdft(1:length(x)/2+1)));
% pause(0.8); hold on;;
% plot(freq,abs(ydft(1:length(x)/2+1)),'r','linewidth',2);
% legend('Original Signal','Bandpass Signal');
% 
% % Design an IIR Butterworth filter of order 10 with 3?dB frequencies of 1 and 1.2 kHz. The sampling frequency is 10 kHz
% d = fdesign.bandpass('N,F3dB1,F3dB2',10,1e3,1.2e3,1e4);
% Hd = design(d,'butter');
% fvtool(Hd)
% 
% % This example requires the DSP System Toolbox software.
% 
% % Design a constrained-band FIR equiripple filter of order 100 with a passband of [1, 1.4] kHz. Both stopband attenuation values are constrained to 60 dB. The sampling frequency is 10 kHz.
% d = fdesign.bandpass('N,Fst1,Fp1,Fp2,Fst2,C',100,800,1e3,1.4e3,1.6e3,1e4);
% d.Stopband1Constrained = true; d.Astop1 = 60;
% d.Stopband2Constrained = true; d.Astop2 = 60;
% Hd = design(d,'equiripple');
% fvtool(Hd);
% measure(Hd)
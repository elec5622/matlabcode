% Ran by read.m

%% Plotting
    
   
    % Uncomment following lines for individual x, y, z directions. This plot made redundant as net 
    % provides the same info.

    % plot(t,x);
    % hold on
    % plot(t,y);plot(t,z);
    % xlabel('Duration of sleep (h)');
    % ylabel('Activity (ms^-^2)');
    % title(tlabel);
    % legend('x','y','z','Location','northeast');
    % savefig(tlabel);
    % print([tlabel],'-dpng'); % save as 'filename.png'

    % Design interval for xticks to illustrate intervals of 1.5 hours
    cycle = 1.5; % standard value taken for length of sleep cycle in hours
    numcycles = ceil(hoursasleep/cycle);
    intervals = linspace(0,numcycles*cycle,numcycles+1);

    figure('units','normalized','outerposition',[0 0 1 1]);    
    h = plot(t,net(1:length(t)),'b');
    min_y = mean(net) - 2;
    max_y = max(net) + 1; 
    max_y = 14; % by observation
    axis([min(t) max(t) min_y max_y]);
    xlabel('Duration of sleep (h)');
    ylabel('Activity (ms^-^2)');
%     legend('net activity','Location','northeast');
    tlabel = ['Sleep Data ' tlabel];
    title(tlabel);
    ax = gca;
    ax.XTick = intervals;
    if clean == 1
        l = legend('Net acceleration','Location','northwest');
    else
        l = legend('Net acceleration','Processed data','Peaks','Location','northwest');
    end
    set(l,'color','w','FontSize',16,'TextColor','black','LineWidth',2) 
%     savefig([tlabel]);
    % print([tlabel],'-dpng'); % save as 'filename.png'

    % max_net = max(net);
    % thres = 1.8*max_net;
    % findpeaks(net,'MinPeakProminence',thres);
    % [pks, locs] = findpeaks(Y,'MinPeakProminence',thres);

    grid on;

    % Alarm control
    % Establish baseline
    % If baseline exceeded, see if time is between +/- 30 mins of alarm. 
    % If not, continue. 
    % If true, alarm goes off. 
    % Background cond: if alarm time + 30 mins is exceeded, alarm anyway.
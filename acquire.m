% Ran by bluetooth.m

% function [data,t] = acquire(comport, duration, interval)
%

%% Open the COM port
% s = serial(comport);
% fopen(b);

try

  %% Set duration of acquisition
  if ~exist('duration', 'var')
    duration = 10;
  end
  if ~exist('interval', 'var')
    interval = 0.5;
  end

  %% Setup the plot
  figure(1), clf
  t = 0:interval:(duration - interval);
  data = zeros(ceil(duration/interval),1);
  h_plot = plot(t,data,'.-');
  xlabel('Time (second)')
  ylabel('Digital output')
  title('ADC output')

  %% Read from the COM port and plot the data
  tstart = tic;
  index = 1;
  while (toc(tstart) < duration)
    % Read data 
    raw = fscanf(b);
    % Convert the string raw data to number
    data(index) = str2double(raw(1:end-2));
    index = index + 1;
    % Update the plot
    figure(1)
    set(h_plot, 'YData', data);
    pause(interval/5);
  end

catch
  fprintf('Interruption - exiting\n')
  fclose(b);
end

%% Close the COM port
fclose(b);

close all; 
clear all;
clc;

do_save = 1; % 1 to save all figures, 0 to skip

% Using t_pause (e.g. 1 second) helps visualise the stages of plotting data: 1. Raw
% RMS accelerometer values; 2. Processed data emphasising peaks; 3. Peak
% detection; 4. Sleep phase classification (light/dark shading for
% light/deep phases, respectively)
t_pause = 1;

check = 0; % check = 1 for debug dialogues
check_durations = 0; % check for duration of phase

clean = 1; % clean = 1 gives simple, user friendly version 

%% Data file set up & looping

directory = 'sleepdatafiles';

allFiles = dir( directory );
allNames = { allFiles.name };

% choose which files to analyse, and remove folders e.g. current ('.') and parent ('..') directory
% Remove folders '.', '..', '.DS_Store' (first 3 entries in cell array) and
% 'Progress log', 'ugly' (last 2 entries)
allNames(1:3) = []; 
allNames(end-1:end) = [];
% Manually select which ones are needed in this run
% allNames(1:end-1) = [];
% allNames(end-4:end) = [];

allNames
for k=1:length(allNames)
    filename=allNames{k};
    dialogue = sprintf('Processing %s',filename);
    disp(dialogue);
%     disp(filename)
    % disp(filename);


    % Handy grab function but not so good for files with "." in them
    % [pathstr,name,ext] = fileparts(filename);

    % Better use regex
    tlabel = regexprep(filename,'[/".csv"]','','ignorecase'); % file name for plot generated

    address = strcat(directory,'/',filename);
    fileID = fopen(address);

    % Find length of file to specify range for individual csvread
    % And also calculate timeframe 
    [status, result] = system( ['wc -l < ', address] );
    numlines = str2num( result );
    % x = csvread(filename,1,4,[1 4 numlines-1 4]);

    resolution = 10; % data points per second, set by Sunny when taking data
    sec2hour = 3600; % seconds per hour
    hoursasleep = numlines/(resolution*sec2hour); % duration of data collection

    sleepdata = csvread(address,1,4); % problem with time formatting due to colons

    % Need to account for shaking around prior sleeping and after waking 
    % (getting device out/in the box). Could design sensing for major shaking.
    % For now, set time exclusion - upper bound of 5 minutes into recording and
    % 5 minutes before end of recording

    exclusion = 5; % 5 minute exclusion zone
    exclusionsec = exclusion * 60;
    cut = exclusionsec * resolution; % this many samples to be cut off from beginning and end
    sleepstart = cut;
    sleepend = numlines - cut;
    t = linspace(1,hoursasleep,numlines - 2 * cut + 1); % time axis

    % To set without cutting
    % sleepstart = 0; sleepend = numlines;
    % t = linspace(1,hoursasleep,numlines - 1);

    % Uncomment if we want motion in individual axes
    % x = sleepdata(sleepstart:sleepend,1);
    % y = sleepdata(sleepstart:sleepend,2);
    % z = sleepdata(sleepstart:sleepend,3);
    net = sleepdata(sleepstart:sleepend,4);

    
    Plotting; % main plot
    pause(t_pause)
    DSPexperiments; % processed data
%     pause(t_pause);
    Peakclassification; % display peaks
    pause(t_pause)
    Shading; % display light/deep sleep
    pause(t_pause)
    
    if clean ~= 1
        hold on; 
        h = plot(t,net(1:length(t)),'b'); % Put the original plot back on top
        hold on; plot(n(1:length(x)),x,'m');
    end

%     uistack(h,'top');
%     set(gcf,'windowbuttonmotionfcn','uistack(hittest,''top'')')
    pause(1)
    if do_save == 1
        folder = 'newpkclass/';
        file = [folder tlabel];
        if clean == 1
            file = [file ' clean'];
        end
        savefig([file '.fig']);
        print(file,'-dpng'); % save as 'filename.png'
    end
    
    pause(t_pause)

end

disp('Finito!')
% handel
close all;
figure
h(1) = plot([1 0 1 0 1],'r','linewidth',20);
hold on; h(2) = plot([0 1 0 1 0],'b','linewidth',20);
for n = 1:5
pause(0.5);
uistack(h(1),'top');
pause(0.5);
uistack(h(2),'top')
end

figure
plot(rand(10,10),'linewidth',10);
set(gcf,'windowbuttonmotionfcn','uistack(hittest,''top'')')